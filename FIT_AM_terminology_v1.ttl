@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix fitam: <http://example.org/ns/fit-am> .

fitam:000001 rdf:type skos:ConceptScheme;
  skos:prefLabel "Terminologie des Föderalen IT-Architekturmanagements"@de;
  skos:prefLabel "Terminology of the Federal IT-Architecture Management"@en;
  skos:definition "Terminologie für architekturrelevante und informationstechnische Themen"@de;
  skos:definition "Terminology for information technology and architecture management"@en;
  owl:versionInfo "1.0";
  dct:title "Terminologie des Föderalen IT-Architekturmanagements"@de;
  dct:title "Terminology of the Federal IT-Architecture Management"@en;
  dct:issued "2024-07-25";
  dct:license <https://creativecommons.org/licenses/by/4.0/>;
  dct:identifier "FITAM_000001" .

fitam:000002 rdf:type skos:Concept;
  skos:prefLabel "OZG-Rahmenarchitektur"@de;
  skos:prefLabel "conceptual framework for architecture management with respect to the Online Access Act (Onlinezugangsgesetz, OZG)"@en;
  skos:definition "Die OZG-Rahmenarchitektur ist ein föderales Vorhaben, das auf der Betrachtungsebene von Nutzendenreisen dazu dienen soll, bei der Erbringung von Verwaltungsleistungen notwendige, zu digitalisierende  Funktionskomplexe zu identifizieren, zu beschreiben und in Zielbilder zu überführen. Dabei ist ein gemeinsames Verständnis von Begrifflichkeiten (Glossar) ebenso notwendig wie auch das Festlegen von Rahmenbedingungen (Architekturprinzipien, Architekturrichtlinien). In weiteren Schritten sind die Funktionskomplexe zu detaillieren und in konkrete Referenzarchitekturen mit dem Ziel zu überführen, Standardisierungsnotwendigkeiten zu erkennen und Lösungskorridore für die operative Umsetzung zu eröffnen."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000002" .
  
fitam:000003 rdf:type skos:Concept;
  skos:prefLabel "Referenzarchitektur"@de;
  skos:prefLabel "reference architecture"@en;
  skos:definition "Eine Referenzarchitektur ist eine idealtypische Architektur für einen definierten Anwendungsbereich. Sie beschreibt implementierungsunabhängig das Verhalten und Zusammenspiel von Architekturbausteinen. Referenzarchitekturen sind Verallgemeinerungen über eine Vielzahl ähnlicher Lösungen für wiederkehrende Problemstellungen. Es handelt sich bei Referenzarchitekturen also nicht um Lösungsarchitekturen, die konkret implementiert werden."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000003" .
  
fitam:000004 rdf:type skos:Concept;
  skos:prefLabel "Fähigkeit"@de;
  skos:prefLabel "capability"@en;
  skos:definition "Eine Fähigkeit beschreibt ein definiertes, inhaltlich abgegrenztes, für die Entwicklung und Umsetzung einer föderalen IT-Strategie erforderliches Vermögen unabhängig von seiner konkreten Implementierung. Fähigkeiten sind untereinander überschneidungsfrei."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000004" .
  
fitam:000005 rdf:type skos:Concept;
  skos:prefLabel "Zielbild"@de;
  skos:prefLabel "objective"@en;
  skos:definition "Ein Zielbild ist die Beschreibung eines zu erreichenden Zielzustands."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000005" .
 
fitam:000006 rdf:type skos:Concept;
  skos:prefLabel "Architekturprinzip"@de;
  skos:prefLabel "architectural principle"@en;
  skos:definition "Architekturprinzipien sind Grundsätze für den Entwurf und die Entwicklung von Architekturen. Sie orientieren sich an den Zielen einer Organisation und helfen, Architekturentscheidungen systematisch und effizient zu treffen. Dadurch werden Entscheidungen beschleunigt, Fehlentscheidungen reduziert und wiederkehrende Grundsatzdiskussionen vermieden. Im Dialog der unterschiedlichen Akteure schaffen sie ein gemeinsames Verständnis über die Ausrichtung der Architektur und begünstigen einheitliche und nachvollziehbare Entscheidungsprozesse."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000006" . 
  
fitam:000007 rdf:type skos:Concept;
  skos:prefLabel "Architekturrichtlinie"@de;
  skos:prefLabel "architectural guidelines"@en;
  skos:definition "Eine Architekturrichtlinie ist die Sammlung von Architekturprinzipien und anderen Architekturvorgaben, die für die Gestaltung und Implementierung von IT-Systemen anzuwenden sind. Die Architekturrichtlinie dient als zentrales Steuerungsinstrument der Architekturentwicklung und wird kontinuierlich weiterentwickelt, um sicherzustellen, dass die IT-Initiativen der Organisation den geltenden Vorgaben wie Standards, Architekturprinzipien, Referenzarchitekturen, Sicherheitsanforderungen usw. entsprechen."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000007" . 
  
fitam:000008 rdf:type skos:Concept;
  skos:prefLabel "Ende-zu-Ende-Digitalisierung"@de;
  skos:prefLabel "end-to-end digitization"@en;
  skos:definition "Eine Ende-zu-Ende-Digitalisierung beschreibt die vollständig digitale Umsetzung von Prozessketten ohne Medienbruch vom Prozessstart bis zum Prozessende. Dabei werden auch Kommunikation zwischen allen Akteuren und deren Beistellungspflichten digital erbracht."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000008" .
  
fitam:000009 rdf:type skos:Concept;
  skos:prefLabel "Medienbruchfreiheit"@de;
  skos:prefLabel "without media disruption"@en;
  skos:definition "Medienbruchfreiheit bedeutet in der Informationsverarbeitung, dass in der Übertragungskette eines Prozesses Daten bzw. Informationen nicht von einem auf ein weiteres/anderes Informationsmedium übertragen werden (müssen), z.B. von einem IT-System auf Papier."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000009" .
  
fitam:000010 rdf:type skos:Concept;
  skos:prefLabel "Qualitätsstandard"@de;
  skos:prefLabel "quality standard"@en;
  skos:definition "Qualitätsstandards sind definierte Vorgaben zur Herstellung oder Beschaffenheit eines Produkts oder zur Erbringung einer Dienstleistung."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000010" .
  
 
fitam:000011 rdf:type skos:Concept;
  skos:prefLabel "Qualitätssicherung"@de;
  skos:prefLabel "quality assurance"@en;
  skos:definition "Unter Qualitätssicherung werden Methoden und Vorgehensweisen verstanden, die die Berücksichtigung und Einhaltung von vorher definierten Standards und deren Kriterien im Herstellungs- oder Pflegeprozess sicherstellen."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000011" .

 
fitam:000012 rdf:type skos:Concept;
  skos:prefLabel "Architektur"@de;
  skos:prefLabel "architecture"@en;
  skos:definition "Die Architektur eines Systems beschäftigt sich mit dessen Grundkonzepten,  verkörpert durch Eigenschaften und Beziehungen der Systemkomponenten untereinander und mit ihrer Umwelt sowie den Prinzipien des Systementwurfs und seiner Evolution."@de;
  dct:created "2024-07-25";
  dct:identifier "FITAM_000012" .  

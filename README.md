# Terminologie des Föderalen IT-Architekturmanagements

Diese Terminologie gilt für das föderale IT-Architekturmanagement des IT-Planungsrates.

#### Redaktionsprozess

Für die Pflege des Glossars wird ein abgestimmter Prozess im FIT-AM benötigt, der wie folgt vorschlagen wird:

- 1.) Die aktuelle Fassung der Terminologie wird im **SKOS Format auf opencode** veröffentlicht. Für eine nutzerfreundliche Darstellung arbeitet die FITKO an einer Bereitstellung der Terminlogie in docosaurus (Tabellenform, Begriff/ Definition) oder Widoco (https://github.com/dgarijo/Widoco).
- 2.) In der veröffentlichten Version werden zunächst nur **Begriffe und ihre Definition auf Deutsch und Englisch** bereitgestellt. Jeder Begriff erhält einen eindeutigen Identifier.
- 3.) Alle Mitglieder des FIT-AM dürfen in einem bestimmen Zeitraum (3 Monate) kommentieren und Änderungen vorschlagen. Dazu werden Tickets im bereitgestellten [Kanban-Board](https://gitlab.opencode.de/it-planungsrat/fit-ab/glossary/-/boards) angelegt. Beim Anlegen der Tickets werden einige Informationen abgefragt. Es wird gebeten sich an existierenden Definitionen (siehe Quellen unten) zu orientieren. Alle neu angelegten Tickets werden im Status **"offen"** geführt.
- 4.) Das **FITKO-AM** führt eine **Vorprüfung** über alle angelegten Tickets durch (z.B. betrifft das Ticket den Themenbereich des FIT-AM, Dopplung usw.). Alle geprüften und für OK befundenen Tickets werden in den Status **"In Abstimmung"** geschoben.
- 5.) **Vertreter des FIT-AM** (Themen-Owner, kleine Fachgruppe) trifft sich quartalsweise und diskutiert und entscheidet. Alle abgenommen Tickets werden in den Status **"Abgestimmt"** überführt. Alle Tickets mit Diskussionsbedarf gehen wieder in den Status "offen".
- 6.) Alle abgestimmten Tickets werden von einem **Redakteur** in das **Redaktionssystem** (Testinstanz Vocbench) gepflegt und eine n**eue Version auf opencode** bereitgestellt. Die abgeschlossen Tickets erhalten dann den Status **"closed"**.
- 7.) Die Fachgruppe (Themen-Owner) werden vom FIT-AB für 12 Monate ernannt.

#### Offene Punkte

Wir benötigen eine **Base-URL** für permanente Identifikatoren, z.B. `https://w3id.org`. Dahinter können wir dann unser konkretes Projektkürzel hängen, also FIT-AM: `https://w3id.org/fit-am`.

#### Quellen für Begriffe

_copy proudly, zumindest referenzieren (exactMatch | closeMatch | broadMatch)_

- [FITKO-AM Glossar](https://preview.docs.fitko.dev/arc/!24/glossary/)
- [EIF](https://ec.europa.eu/isa2/eif_en/)
- [EIRA](https://joinup.ec.europa.eu/collection/european-interoperability-reference-architecture-eira/solution/eira-ontology)
- [ISO](https://www.iso.org/)
- [TOGAF](https://pubs.opengroup.org/togaf-standard/introduction/chap04.html)
- [Fähigkeitenlandkarte](https://www.fitko.de/fileadmin/fitko/foederale-koordination/gremienarbeit/Foederales_IT-Architekturboard/Fähigkeitenlandkarte_v1.0.pdf)
- [Architekturrichtlinie des Bundes](https://www.cio.bund.de/SharedDocs/downloads/Webs/CIO/DE/digitaler-wandel/architekturen-standard/ArchRL.pdf?__blob=publicationFile&v=9)
- [Föderale IT-Architekturrichtlinien](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)

#### Styleguide(s) und Policies für die Terminologie-Modellierung

* https://semiceu.github.io/style-guide/1.0.0/index.html
* http://obofoundry.org/principles/fp-012-naming-conventions.html
* http://obofoundry.org/principles/fp-000-summary.html

#### Beispiele

* _High Value Dataset_ Kategorien der EU: https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/high-value-dataset-category

letzte Änderung: 04.10.2024

_Es müssen nicht alle Felder gefüllt werden. Je mehr, desto besser - hilft dem Themenowner und Redaktionsteam beim Verstehen und Entscheiden._

## Problembeschreibung/Anliegen

## Begründung

## Welche Version ist von dem Problem betroffen?

## Gibt es für das Anliegen einen passenden Eintrag in anderen Quellen ([siehe Liste](https://gitlab.opencode.de/it-planungsrat/fit-ab/glossary/-/blob/main/README.md) )? 

#### Wenn ja, welchen (bitte Link angeben)?

#### Wenn nein, bitte eine Begründung angeben 

## bitte ein Beispiel angeben

## Begriff in Deutsch

## Begriff in Englisch

## Beschreibung auf Deutsch

## Beschreibung auf Englisch

## Kontakt
